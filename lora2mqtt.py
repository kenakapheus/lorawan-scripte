#!/usr/bin/python3

import socket
import struct
import json
import base64
import binascii
import argparse
import paho.mqtt.client as mqtt


parser = argparse.ArgumentParser(description='Forward LoRaWAN Pakets as MQTT Messages')
parser.add_argument('--host', default='mqtt', help='MQTT server address. Default is "mqtt"')
parser.add_argument('--port', '-p', default='1700', type=int, help='Port to listen for LoRaWAN UDP Pakets. Default is 1700')
parser.add_argument('--topic', '-t',  default='lora/gateway/', help='Topic prefix, the gateway id will be appended. Default is "lora/gateway/"')

args=parser.parse_args()


UDP_IP = "0.0.0.0"
UDP_PORT = args.port

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

client = mqtt.Client()

client.connect(args.host)

client.loop_start()

def parse_upd_packet(packet):
    proto_version, random_token, cmd, gw = struct.unpack("<BHB8s", packet[:12])
    pld = json.loads(packet[12:]) if len(packet) > 12 else {}
    return {"version": proto_version, "token": random_token, "command": cmd, "gateway": binascii.hexlify(gw).decode(), "payload": pld}

while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    #print(data)
    pld = parse_upd_packet(data)
    ip, port = addr
    response = struct.pack("<BHB", pld["version"], pld["token"], 0x04 if pld["command"] == 0x02 else 0x01)
    r = sock.sendto(response, (ip, 1700))
    #print("received message:", pld)
    client.publish(f"{args.topic}{pld['gateway']}", json.dumps(pld))
    
