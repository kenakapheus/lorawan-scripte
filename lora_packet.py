import struct
import bitstruct
import base64
from Crypto.Hash import CMAC
from Crypto.Cipher import AES


def parse_udp_packet(packet):
    proto_version, random_token, cmd, gw = struct.unpack("<BHB8s", packet[:12])
    return {"version": proto_version, "token": random_token, "command": cmd, "gateway": gw, "payload": packet[12:]}

def pack_adp_packet(proto_version, random_token, cmd, gw, payload):
    packet = struct.pack("<BHB8s", proto_version, random_token, cmd, gw)
    return packet + payload

def calculate_mic(key, message, direction, f_cnt, dev_addr):
    B0 = struct.pack("<BLB4sLBB", 0x49, 0x00, direction, bytes(dev_addr), f_cnt, 0, len(message))
    #print("B-Vec:              "+"".join([f"{x:02X}" for x in B0]))
    #print("Packet:             "+"".join([f"{x:02X}" for x in message]))
    return CMAC.CMAC(bytes(key), msg=B0 + message, ciphermod=AES).digest()[0:4]

def parse_phy_payload(packet):
    mtype, rfu, major = bitstruct.unpack("u3u3u2", packet[:1])
    mic, = struct.unpack("<4s", packet[-4:])
    return {"MHDR": {"mtype": mtype, "rfu": rfu, "major": major}, "mic": mic, "payload": packet[1:-4]}

def pack_phy_payload(mtype, payload, network_session_key, f_cnt):
    packet = bitstruct.pack("u3u3u2", mtype, 0, 0) + payload
    mic = calculate_mic(network_session_key, packet, 0, f_cnt)
    return packet + mic 
    
mtypes = [
    "Join Request",
    "Join Accept",
    "Unconfirmed Data Up",
    "Unconfirmed Data Down",
    "Confirmed Data Up",
    "Confirmed Data Down",
    "RFU",
    "Proprietary"
]

def parse_mac_payload(packet):
    dev_addr, fctrl, fcnt = struct.unpack("<4ssH", packet[:7])
    payload = packet[7:]
    adr, adrackreq, ack, classb, fopts_len = bitstruct.unpack("u1u1u1u1u4", fctrl)
    if fopts_len:
        fopts = payload[:fopts_len]
        payload = payload[fopts_len:]
    else:
        fopts = None
    if len(payload) > 0:
        fport, = struct.unpack("<B", payload[:1])
        payload = payload[1:]
    else:
        fport = None

    return {"DevAddr": dev_addr, "FCtrl": {"ADR": adr, "ADRACKReq": adrackreq, "ACK": ack, "ClassB": classb}, "FCnt": fcnt, "FOpts": fopts, "FPort": fport, "payload": payload}

def pack_mac_payload(fcnt, adr, adrackreq, ack, classb, fopts, fport, payload, dev_addr):
    fctrl = bitstruct.pack("u1u1u1u1u4", adr, adrackreq, ack, classb, len(fopts))
    packet = struct.pack("<4ssH", bytes(dev_addr), fctrl, fcnt) + bytes(fopts)
    return packet + struct.pack("<B", fport) + payload


def encrypt_payload(key, direction, f_cnt, payload, dev_addr):
    pld_len = len(payload)
    a_vec = [struct.pack("<BLB4sLBB", 0x01, 0x00, direction, bytes(dev_addr), f_cnt, 0x00, i + 1) for i in range(ceil(pld_len/16))]
    #for a in a_vec:
    #    print("A-Vec:              "+"".join([f"{x:02X}" for x in a]))
    s = b''.join([AES.new(bytes(key)).encrypt(a) for a in a_vec])
    #print("S-Vec:              "+"".join([f"{x:02X}" for x in s]))
    pld = payload + bytes(pld_len % 16)
    r = bytes([pld[i] ^ s[i] for i in range(pld_len)])
    return r[:pld_len]

