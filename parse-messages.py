#!/usr/bin/python3

import paho.mqtt.client as mqtt
import argparse
import json
import base64
import lora_packet
import binascii


parser = argparse.ArgumentParser(description='Parse LoRaWAN Packets from MQTT')
parser.add_argument('--host', default='mqtt', help='MQTT server address. Defaults to "mqtt"')
parser.add_argument('--topic', '-t',  default='lora/gateway/#', help='Topic to subscribe to. Default is "lora/gateway/#"')

args=parser.parse_args()

def parse_lora_packet(packet):
  if packet['payload']:
    try:
      if packet["command"] == 0x00: # PUSH_DATA
        decoded_message = packet["payload"]
        if "rxpk" in decoded_message:
            for rf_message in decoded_message["rxpk"]:
                phy_packet = base64.b64decode(rf_message["data"])
                phy_message = lora_packet.parse_phy_payload(phy_packet)
                mac_message = lora_packet.parse_mac_payload(phy_message["payload"])
                #print(phy_message)
                print(f"Gateway: {packet['gateway']} -> CR:{rf_message['codr']:4s}{rf_message['datr']:>10s}@ch{rf_message['chan']:<2d} ({rf_message['freq']:5.1f}MHz) RSSI:{rf_message['rssi']:4d}dBm SNR:{rf_message['lsnr']:5.1f}dB", end="")
                print(f" -> {lora_packet.mtypes[phy_message['MHDR']['mtype']]:21s} Dev:{binascii.hexlify(mac_message['DevAddr']).decode()}", end="")
                if mac_message['FPort'] != None:
                  print(f" Port:{mac_message['FPort']:<5d}", end="")
                if mac_message['payload']:
                  print(f" {mac_message['payload']}", end="")
                print()
                
    except Exception as e:
      print(e)
  

def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))
	client.subscribe([(args.topic, 0)])

def on_message(client, userdata, msg):
  parse_lora_packet(json.loads(msg.payload.decode()))

client = mqtt.Client()

client.on_connect = on_connect
client.on_message = on_message

client.connect(args.host)

client.loop_forever()

